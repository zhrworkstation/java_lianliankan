import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Graphics;
public class Sider extends GameControl{


	public boolean IsOK;
	
	BufferedImage btn1=null;
	public List<GameButton> buttons=null;


	Sider(BufferedImage img,int x,int y){
		super(img,x,y);
		Visible=false;
		IsOK=false;
		buttons = new ArrayList<GameButton>();
		try{
			btn1=ImageIO.read(new File("image/btn1.png"));
		}
		catch(Exception e){
			System.out.println(e);
		}
		for(int i=0;i<5;i++){
			GameButton btn=new GameButton(btn1,x,y+i*50);
			btn.Width=200;
			btn.Height=50;
			btn.Visible=true;
			buttons.add(btn);
		}
		Height=660;
		Width=200;

	}
	public void draw(Graphics g){

		if(Visible){
			show();
			
				for(int i=0;i<buttons.size();i++){
					g.drawImage(buttons.get(i).img,x,buttons.get(i).y,buttons.get(i).Width,buttons.get(i).Height,null);
				
			}
		}
		else{
			hide();
			for(int i=0;i<buttons.size();i++){
					g.drawImage(buttons.get(i).img,x,buttons.get(i).y,buttons.get(i).Width,buttons.get(i).Height,null);
				}
		}
	}
	public void show(){
		if(!IsOK){

			x=x+10;
			if(x==0){
				IsOK=true;
			}

		}
	}
	public void hide(){
		if(IsOK){

			x=x-10;
			if(x==-200){
				IsOK=false;
			}

		}
	}
	



}


